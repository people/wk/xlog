/*

   xlog - GTK+ logging program for amateur radio operators
   Copyright (C) 2012 - 2013 Andy Stewart <kb1oiq@arrl.net>
   Copyright (C) 2001 - 2010 Joop Stakenborg <pg4i@amsat.org>

   This file is part of xlog.

   Xlog is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Xlog is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with xlog.  If not, see <http://www.gnu.org/licenses/>.

*/

/*
 * callbacks_mainwindow.c - callbacks for the main window
 *
 * these are callbacks not part of the menu, qsoframe, toolbar or list.
 */

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <string.h>
#include <hamlib/rig.h>

#include "hamlib-utils.h"
#include "callbacks_mainwindow.h"
#include "callbacks_mainwindow_toolbar.h"
#include "callbacks_mainwindow_menu.h"
#include "gui_savedialog.h"
#include "support.h"
#include "gui_utils.h"
#include "utils.h"
#include "cfg.h"
#include "log.h"
#include "gui_netkeyer.h"
#include "netkeyer.h"
#include "main.h"
#include "globals.h"

extern GtkWidget *keyerwindow;
extern preferencestype preferences;
extern keyerstatetype keyerstate;
extern GList *logwindowlist;
extern GtkUIManager *ui_manager;

/* capture the delete event and display a warning in case the log
 * has not been saved */
gboolean
on_mainwindow_delete_event (GtkWidget * widget, GdkEvent * event,
					gpointer user_data)
{
	gint i;
	logtype *logwindow;
	gboolean logchanged = FALSE;

	for (i = 0; i < g_list_length (logwindowlist); i++)
	{
		logwindow = g_list_nth_data (logwindowlist, i);
		if (logwindow->logchanged) logchanged = TRUE;
	}

	if (logchanged)
		create_savedialog ();
	else
	{
		if (preferences.areyousure > 0)
			create_exitdialog ();
		else
		{
			save_windowsize_and_cleanup ();
			gtk_main_quit ();
		}

	}
	return (TRUE);
}

/* switch between pages of the notebook */
void
on_mainnotebook_switch_page (GtkNotebook * notebook, GtkNotebookPage * page,
					 gint page_num, gpointer user_data)
{
	logtype *logw = NULL;

	while (page_num >= 0)
	{
		logw = g_list_nth_data (logwindowlist, page_num);
		if (logw) break;
		page_num--;
	}

	if (logw)
	{
		set_qsoframe (logw);
	}
}


/* Helper for on_mainwindow_keypress.  */
static void
activate_keyer_widget (const char *name)
{
        GtkWidget *abutton;

	if (keyerwindow)
        {
                abutton = lookup_widget (keyerwindow, name);
                gtk_widget_activate (abutton);
        }
}


/* Handle key GDK_Return in non-keyer mode.  */
static void
handle_key_return (void)
{
	GtkWidget *callentry, *rstentry;
        GtkWidget *abutton;

        callentry = lookup_widget (mainwindow, "callentry");
        if (gtk_widget_has_focus(callentry))
        {
                rstentry = lookup_widget (mainwindow, "rstentry");
                gtk_widget_grab_focus (rstentry);
        }
        else if ((abutton = try_lookup_widget
                  (mainwindow, "qsosavebutton"))
                 && gtk_widget_has_focus (abutton))
        {
                gtk_button_clicked (GTK_BUTTON (abutton));
        }
}


/* Handle key GDK_Return in keyer mode.  */
static void
handle_keyer_key_return (void)
{
	GtkWidget *callentry, *rstentry, *myrstentry, *count;
        GtkWidget *abutton;
        GtkWidget *clickallmenu;
	gchar *call, *countstr, *str;
	gint c;

#define CQ 13
#define SP 14

	if (!keyerwindow)
                return;

        myrstentry = lookup_widget (mainwindow, "myrstentry");
        callentry = lookup_widget (mainwindow, "callentry");
        call = gtk_editable_get_chars (GTK_EDITABLE (callentry), 0, -1);

        if (!GTK_WIDGET_HAS_FOCUS (callentry) && (strlen (call) == 0))
        {       // Ubuntu bug #608718: do not log QSO without callsign
                // Thanks John Nogatch for the patch.
                gtk_widget_grab_focus (callentry);
        }
        else if (gtk_widget_has_focus (myrstentry))
        {
                if (keyerstate.cqmode)
                        cw (NULL, GINT_TO_POINTER(CQ));
                else
                        cw (NULL, GINT_TO_POINTER(SP));
                clickallmenu = gtk_ui_manager_get_widget
                        (ui_manager, "/MainMenu/EditMenu/Click All");
                g_signal_emit_by_name (G_OBJECT (clickallmenu), "activate");
                abutton = gtk_ui_manager_get_widget (ui_manager,
                                                     "/MainMenu/EditMenu/Write");
                g_signal_emit_by_name (G_OBJECT (abutton), "activate");
        }
        else if (gtk_widget_has_focus (callentry))
        {
                rstentry = lookup_widget (mainwindow, "rstentry");
                if (strlen (call) > 0)
                {
                        if (keyerstate.cqmode)
                                activate_keyer_widget ("f3button");
                        else
                                activate_keyer_widget ("f6button");

                        /* fill in defaults, check if we use a counter */
                        if (strlen (preferences.defaulttxrst) > 0)
                        {
                                if (g_strrstr (preferences.defaulttxrst, "#"))
                                {
                                        count = lookup_widget (keyerwindow, "count");
                                        c = gtk_spin_button_get_value (GTK_SPIN_BUTTON (count));
                                        if (c < 10)
                                                countstr = g_strdup_printf ("00%d", c);
                                        else if (c < 100)
                                                countstr = g_strdup_printf ("0%d", c);
                                        else
                                                countstr = g_strdup_printf ("%d", c);
                                        str = my_strreplace (preferences.defaulttxrst, "#",
                                                             countstr);
                                        g_free (countstr);
                                        gtk_entry_set_text (GTK_ENTRY (rstentry), str);
                                        g_free (str);
                                }
                                else
                                        gtk_entry_set_text (GTK_ENTRY (rstentry),
                                                            preferences.defaulttxrst);
                        }
                        if (strlen (preferences.defaultrxrst) > 0)
                                gtk_entry_set_text (GTK_ENTRY (myrstentry),
                                                    preferences.defaultrxrst);

                        gtk_widget_grab_focus (myrstentry);
                        gtk_editable_set_position (GTK_EDITABLE(myrstentry), -1);
                }
                else
                {
                        if (keyerstate.cqmode) /* call CQ */
                                activate_keyer_widget ("f1button");
                        else
                                activate_keyer_widget ("f6button");
                }
        }
        else if ((abutton = try_lookup_widget
                  (mainwindow, "qsosavebutton"))
                 && gtk_widget_has_focus (abutton))
        {
                gtk_button_clicked (GTK_BUTTON (abutton));
        }
}


/* Handle the keys GDK_Up and GDK_down.  QSY gives the frequency
 * differenz.  Return true if a change was done.  */
static gboolean
handle_key_updown (GdkEventKey *event, int qsydiff)
{
        if ((preferences.hamlib > 0) && (event->state & GDK_CONTROL_MASK))
        {
                if (preferences.polltime == 0)
                        get_frequency ();
                set_frequency (programstate.rigfrequency + qsydiff);
                return TRUE;
        }

        return FALSE;
}


/* catch keypresses when keyer is active, don't use PgUp/PgDn here */
gboolean
on_mainwindow_keypress (GtkWidget *widget, GdkEventKey *event, gpointer data)
{
	if (keyerwindow)
        {
                switch (event->keyval)
                {
                case GDK_F1: activate_keyer_widget ("f1button"); break;
                case GDK_F2: activate_keyer_widget ("f2button"); break;
                case GDK_F3: activate_keyer_widget ("f3button"); break;
                case GDK_F4: activate_keyer_widget ("f4button"); break;
                case GDK_F5: activate_keyer_widget ("f5button"); break;
                case GDK_F6: activate_keyer_widget ("f6button"); break;
                case GDK_F7: activate_keyer_widget ("f7button"); break;
                case GDK_F8: activate_keyer_widget ("f8button"); break;
                case GDK_F9: activate_keyer_widget ("f9button"); break;
                case GDK_F10:activate_keyer_widget ("f10button"); break;
                case GDK_F11:activate_keyer_widget ("f11button"); break;
                case GDK_F12:activate_keyer_widget ("f12button"); break;
                case GDK_Escape: activate_keyer_widget ("stopbutton"); break;
                case GDK_Return:
                        handle_keyer_key_return ();
                        break;
                case GDK_Up:
                        if (!handle_key_updown (event, 50))
                                return FALSE;
                        break;
                case GDK_Down:
                        if (!handle_key_updown (event, -50))
                                return FALSE;
                        break;

                default:
                        return FALSE;
                }
        }
	else
        {
                switch (event->keyval)
                {
                case GDK_Return:
                        handle_key_return ();
                        break;
                case GDK_Up:
                        if (!handle_key_updown (event, 50))
                                return FALSE;
                        break;
		case GDK_Down:
                        if (!handle_key_updown (event, -50))
                                return FALSE;
                        break;
		default:
                        return FALSE;
                }
	}

	return TRUE;
}
