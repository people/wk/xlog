/*

   xlog - GTK+ logging program for amateur radio operators
   Copyright (C) 2001 - 2008 Joop Stakenborg <pg4i@amsat.org>
   Copyright (C) 2016 Andy Stewart <kb1oiq@arrl.net>
   This file is part of xlog.

   Xlog is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Xlog is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with xlog.  If not, see <http://www.gnu.org/licenses/>.

*/

/*
 * callbacks_mainwindow_qsoframe.c - callbacks for the QSO frame, where all
 * the entry widgets are ...
 */

#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>
#include <sys/types.h>

#if HAVE_SYS_SHM_H
#include <sys/ipc.h>
#include <sys/shm.h>
#endif

#include <errno.h>
#include <string.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <hamlib/rig.h>

#include "callbacks_mainwindow_qsoframe.h"
#include "support.h"
#include "dxcc.h"
#include "cfg.h"
#include "xlog_enum.h"
#include "utils.h"
#include "gui_utils.h"
#include "log.h"
#include "wwl.h"
#include "gui_netkeyer.h"
#include "main.h"
#include "hamlib-utils.h"
#include "gui_mainwindow.h"
#include "callbacks_mainwindow_toolbar.h"
#include "globals.h"


/* defines and variables for shared memory support */
#define KEY 6146
#define SHMSIZE	40
void *shareCall;

extern GtkWidget *b4window, *keyerwindow;
extern gint callid;
extern programstatetype programstate;
extern GList *logwindowlist;
extern preferencestype preferences;
extern GList *logwindowlist;

/* Flags declaring the match status of updateb4window.  */
#define WB4FLAG_MATCH_ANY       1  /* A match in any log.  */
#define WB4FLAG_MATCH_CURRENT   2  /* A match in the current log.  */
#define WB4FLAG_EXACT_ANY       4  /* An exact match in any log.  */
#define WB4FLAG_EXACT_CURRENT   8  /* An exact match in the current log.  */
#define WB4FLAG_EXACT_SAMEBAND 16  /* Exact match on the same band.  */
#define WB4FLAG_MAYBE_CALLSIGN 128 /* This looks like a callsign.  */

/* Return true if the widget with NAME is visible.  */
static gboolean
is_widget_visible (const char *name)
{
        GtkWidget *widget;
        widget = lookup_widget (mainwindow, name);
        return gtk_widget_get_visible (widget);
}


/* Return a malloced string with the content of the ENTRY.  This
 * function never returns NULL.  */
static char *
entry_get_text (const char *name)
{
        GtkWidget *entry;
        char *text;

        entry = lookup_widget (mainwindow, name);
        text = gtk_editable_get_chars (GTK_EDITABLE (entry), 0, -1);
        if (!text)
        {
                g_warning ("%s failed for '%s'", __func__, name);
                text = g_strdup ("");
        }

        return text;
}


/* Set TEXT into the entry NAME.  */
#if 0 /* Not yet used.  */
static void
entry_set_text (const char *name, const char *text)
{
        GtkWidget *entry;

        entry = lookup_widget (mainwindow, name);
        if (!entry)
                g_warning ("%s failed for '%s'", __func__, name);
        else
                gtk_entry_set_text (GTK_ENTRY (entry), text);
}
#endif /*not yet used*/


/* If the entry with NAME is blank, insert DEFTEXT into it.  */
static void
entry_set_default_text (const char *name, const char *deftext)
{
        GtkWidget *entry;
        char *text;

        entry = lookup_widget (mainwindow, name);
        text = gtk_editable_get_chars (GTK_EDITABLE (entry), 0, -1);
        if (!text)
                g_warning ("%s failed for '%s'", __func__, name);
        else
        {
                g_strstrip (text);
                if (!*text)
                        gtk_entry_set_text (GTK_ENTRY (entry), deftext);
                g_free (text);
        }
}



/* QSO FRAME */

/* Get current date and time and fill in the date- and gmtentry.  */
void
on_gmtbutton_clicked (GtkButton * button, gpointer user_data)
{
        GtkWidget *entry;
	gchar *nowtime, *nowdate;

        nowtime = xloggettime (&nowdate);
 	entry = lookup_widget (mainwindow, "gmtentry");
 	gtk_entry_set_text (GTK_ENTRY (entry), nowtime);
 	g_free (nowtime);
	entry = lookup_widget (mainwindow, "dateentry");
	gtk_entry_set_text (GTK_ENTRY (entry), nowdate);
	g_free (nowdate);
}


static void
insert_time (void)
{
        on_gmtbutton_clicked (NULL, NULL);
}


void
on_endbutton_clicked (GtkButton * button, gpointer user_data)
{
	GtkWidget *entry;
	gchar *nowtime;

	entry = lookup_widget (mainwindow, "endentry");
	nowtime = xloggettime (NULL);
	gtk_entry_set_text (GTK_ENTRY (entry), nowtime);
	g_free (nowtime);
}

/* convert callsign to uppercase */
void
on_callentry_insert_text (GtkEditable * editable, gchar * new_text,
	gint new_text_length, gpointer position, gpointer user_data)
{
	gchar *result;
        GtkWidget *widget;

        /* First remove the warning icon.  */
        gtk_entry_set_icon_from_stock (GTK_ENTRY (editable),
                                       GTK_ENTRY_ICON_SECONDARY,
                                       NULL);

        /* Check whether a space was inserted and jump to the next
         * field in this case.  */
        if ((new_text_length >= 1 || new_text_length == -1)
            && *new_text == ' ')
        {

                g_signal_stop_emission_by_name (GTK_OBJECT (editable),
                                                "insert-text");
                widget = lookup_widget (mainwindow, "rstentry");
                gtk_widget_grab_focus (widget);
                return;
        }

        result = g_ascii_strup (new_text, new_text_length);

        /* Insert the new callsign and block insert_text, so we don't
         * call this twice */
	g_signal_handlers_block_by_func
		(GTK_OBJECT (editable),	on_callentry_insert_text, user_data);
	gtk_editable_insert_text (editable, result, new_text_length, position);
	g_signal_handlers_unblock_by_func
		(GTK_OBJECT (editable), on_callentry_insert_text, user_data);
	g_signal_stop_emission_by_name (GTK_OBJECT (editable), "insert_text");

	g_free (result);
}



static void
do_on_rstentry_insert_text (GtkEditable *editable, gchar *new_text,
                            gint new_text_length, gpointer position,
                            gpointer user_data, void *funcname)
{
        int n;
        char *result = NULL;

        /* Remove the warning icon.  */
        gtk_entry_set_icon_from_stock (GTK_ENTRY (editable),
                                       GTK_ENTRY_ICON_SECONDARY,
                                       NULL);

        /* Translate common CW abbreviations.  We translate only
         * lowercase letters so that that the uppercase letters can be
         * used to override it.  FIXME: Might be better to do this
         * only in CW mode.  */
        for (n=0; n < new_text_length; n++)
                if (!g_ascii_isdigit (new_text[n]))
                        break;
        if (new_text_length && n < new_text_length)
        {
                result = g_malloc (new_text_length + 1);
                for (n=0; n < new_text_length; n++)
                {
                        switch (new_text[n])
                        {
                        case 'a': result[n] = '1'; break;
                        case 'e': result[n] = '5'; break;
                        case 'n': result[n] = '9'; break;
                        case 't': result[n] = '0'; break;
                        default: result[n] = new_text[n]; break;
                        }
                }
                result[n] = 0;
        }


        if (result)
        {
                /* Insert the changed text and block insert_text, so
                 * we don't call us twice.  */
                g_signal_handlers_block_by_func (GTK_OBJECT (editable),
                                                 funcname, user_data);
                gtk_editable_insert_text (editable,
                                          result, new_text_length, position);
                g_signal_handlers_unblock_by_func (GTK_OBJECT (editable),
                                                   funcname, user_data);
                g_signal_stop_emission_by_name (GTK_OBJECT (editable),
                                                "insert_text");
                g_free (result);
        }
}


void
on_rstentry_insert_text (GtkEditable *editable, gchar *new_text,
	gint new_text_length, gpointer position, gpointer user_data)
{

        do_on_rstentry_insert_text (editable, new_text, new_text_length,
                                    position, user_data,
                                    on_rstentry_insert_text);
}


void
on_myrstentry_insert_text (GtkEditable *editable, gchar *new_text,
	gint new_text_length, gpointer position, gpointer user_data)
{
        do_on_rstentry_insert_text (editable, new_text, new_text_length,
                                    position, user_data,
                                    on_myrstentry_insert_text);
}


/* convert awards entry to uppercase */
void
on_awardsentry_insert_text (GtkEditable * editable, gchar * new_text,
	gint new_text_length, gpointer position, gpointer user_data)
{
	gchar *result;

        result = g_ascii_strup (new_text, new_text_length);

	g_signal_handlers_block_by_func
		(GTK_OBJECT (editable),	on_awardsentry_insert_text, user_data);
	gtk_editable_insert_text (editable, result, new_text_length, position);
	g_signal_handlers_unblock_by_func
		(GTK_OBJECT (editable), on_awardsentry_insert_text, user_data);
	g_signal_stop_emission_by_name (GTK_OBJECT (editable), "insert_text");
	g_free (result);
}

void
on_callbutton_clicked (GtkButton * button, gpointer user_data)
{
	char *call;

        call = entry_get_text ("callentry");
	if (strlen(call) > 2)
	{
		if (g_strrstr (preferences.openurl, "<call>"))
		{
			gchar *link = g_strdup (preferences.openurl);
			link = my_strreplace (link, "<call>", call);
			open_url (link);
			g_free (link);
		}
	}
	g_free (call);
}


/* Helper for updateb4window.  Returns a bit vector:
 *  Bit 0 - Any match
 *  Bit 1 - Exact match
 *  Bit 2 - Exact match on the same band.
 */
static gboolean
updateb4_one_log (const char *callsign, int band,
                  logtype *logw, GtkTreeModel *model, GtkTreeIter *iter,
                  GtkListStore *b4store, GtkTreeIter *b4iter)
{
        char *thiscallsign, *thisbandstring;
        int thisband;
        int j;
        unsigned int result = 0;
	GtkTreeViewColumn *column;
        char *string;

        gtk_tree_model_get (model, iter,
                            CALL, &thiscallsign,
                            BAND, &thisbandstring,
                            -1);

        thisband = freq2enum (thisbandstring);
        g_free (thisbandstring);

        if (!g_ascii_strncasecmp (callsign, thiscallsign, strlen (callsign)))
        {
                result |= 1;
                if (!g_ascii_strcasecmp (callsign, thiscallsign))
                {
                        result |= 2;
                        if (band == thisband)
                                result |= 4;
                }

                gtk_list_store_prepend (b4store, b4iter);
                /* The b4window has the same field order as the logs
                 * but with an additional first field for the log
                 * name.  Thus we copy the log name first.  */
                gtk_list_store_set (b4store, b4iter,
                                    0, logw->logname,
                                    -1);
                for (j = 0; j < QSO_FIELDS; j++)
                {
                        column = gtk_tree_view_get_column
                                (GTK_TREE_VIEW(logw->treeview), j);
                        gtk_tree_model_get (model, iter,
                                            j, &string,
                                            -1);
                        if (*string)
                                gtk_list_store_set (b4store, b4iter,
                                                    j+1, string,
                                                    -1);
                        g_free (string);
                }
        }
        g_free (thiscallsign);

        return result;
}


/* Update the Worked Before window.  Returns a combination of the
 * WB4FLAG values.  */
static unsigned int
updateb4window (gchar *callsign)
{
	GtkWidget *b4treeview;
	guint i;
	logtype *logw;
	gboolean valid = FALSE;
	GtkTreeIter iter, b4iter;
	GtkTreeModel *model;
	GtkListStore *b4store;
        int current_page;
        int band;
        unsigned int result = 0;
        unsigned int oneres;

	if (!b4window)
                return 0;

        /* FIXME: The next is only needed to update the window but not
         * for checking for calls.  */
        if (!gtk_widget_get_visible (b4window))
                return 0;  /* FIXME!! */

        /* Clear the list of the widget. */
        b4treeview = lookup_widget (b4window, "b4treeview");
        b4store = GTK_LIST_STORE (gtk_tree_view_get_model
                                  (GTK_TREE_VIEW (b4treeview)));
        gtk_list_store_clear (GTK_LIST_STORE (b4store));

        if (strlen (callsign) < 3)
                return 0; /* for responsiveness */

        /* Fixme: We should have the band instantly available.  */
        if (preferences.bandseditbox == 0)
        {
                GtkWidget *bandoptionmenu = lookup_widget
                        (mainwindow, "bandoptionmenu");
                band = freq2enum (lookup_band
                                  (gtk_combo_box_get_active
                                   (GTK_COMBO_BOX(bandoptionmenu))));
        }
        else
        {
                GtkWidget *bandentry = lookup_widget (mainwindow, "bandentry");
                band = freq2enum (gtk_editable_get_chars
                                  (GTK_EDITABLE (bandentry), 0, -1));
        }

        result |= WB4FLAG_MAYBE_CALLSIGN;

	current_page = gtk_notebook_get_current_page
                (GTK_NOTEBOOK (mainnotebook));

        /* Search all logs and put matching log entries into the
         * b4window list. */
        for (i = 0; i < g_list_length (logwindowlist); i++)
        {
                logw = g_list_nth_data (logwindowlist, i);
                model = gtk_tree_view_get_model (GTK_TREE_VIEW(logw->treeview));
                valid = gtk_tree_model_get_iter_first (model, &iter);
                while (valid)
                {
                        oneres = updateb4_one_log (callsign, band,
                                                   logw, model, &iter,
                                                   b4store, &b4iter);
                        if ((oneres & 1))
                        {
                                result |= WB4FLAG_MATCH_ANY;
                                if (i == current_page)
                                        result |= WB4FLAG_MATCH_CURRENT;
                        }
                        if ((oneres & 2))
                        {
                                result |= WB4FLAG_EXACT_ANY;
                                if (i == current_page)
                                {
                                        result |= WB4FLAG_EXACT_CURRENT;
                                        if ((oneres & 4))
                                              result |= WB4FLAG_EXACT_SAMEBAND;
                                }
                        }

                        valid = gtk_tree_model_iter_next (model, &iter);
                }
        }

        return result;
}


/* Find call in the existisng log and update some info fields if they
 * are not yet set.  This used the first entry found for the call.  */
static void
do_typeaheadfind (const char *call)
{
	guint i;
	GtkWidget *remarksvbox, *remtv;
	gboolean valid;
	gboolean found = FALSE;
	gchar *logcallsign;
        gchar *logname = NULL;
        gchar *logqth  = NULL;
        gchar *logloc  = NULL;
        gchar *logremarks = NULL;
	logtype *logw;
	GtkTreeModel *model;
	GtkTreeIter iter;

	if (strlen (call) < 3)
                return; /* Too short to be useful.  */

        for (i = 0; i < g_list_length (logwindowlist); i++)
        {
                logw = g_list_nth_data (logwindowlist, i);
                model = gtk_tree_view_get_model (GTK_TREE_VIEW(logw->treeview));
                valid = gtk_tree_model_get_iter_first (model, &iter);
                while (valid)
                {
                        gtk_tree_model_get (model, &iter, CALL, &logcallsign, -1);
                        if (!g_ascii_strcasecmp (call, logcallsign))
                        {
                                gtk_tree_model_get (model, &iter,
                                                    NAME, &logname,
                                                    QTH, &logqth,
                                                    LOCATOR, &logloc,
                                                    REMARKS, &logremarks,
                                                    -1);
                                found = TRUE;
                                goto ready;
                        }
                        valid = gtk_tree_model_iter_next (model, &iter);
                }
        }

ready:
        if (found)
        {
                if (is_widget_visible ("namehbox"))
                        entry_set_default_text ("nameentry", logname);
                if (is_widget_visible ("qthhbox"))
                        entry_set_default_text ("qthentry", logqth);
                if (is_widget_visible ("locatorhbox"))
                        entry_set_default_text ("locatorentry", logloc);

                remarksvbox = lookup_widget(mainwindow, "remarksvbox");
                if (strlen (preferences.defaultremarks) == 0
                    && gtk_widget_get_visible (remarksvbox))
                {
                        GtkTextBuffer *b;

                        remtv = lookup_widget(mainwindow, "remtv");
                        b = gtk_text_view_get_buffer (GTK_TEXT_VIEW (remtv));
                        if (!gtk_text_buffer_get_char_count(GTK_TEXT_BUFFER(b)))
                            gtk_text_buffer_set_text (b, logremarks, -1);
                }

        }

        g_free (logname);
        g_free (logqth);
        g_free (logloc);
        g_free (logremarks);
}



void
on_callentry_changed (GtkEditable * editable, gpointer user_data)
{
        int is_new_qso;
	gchar *call;
	guint st, zone, cont, iota;
#if HAVE_SYS_SHM_H
	gchar *errorstr;
#endif
        GdkColor *color;
        unsigned int wb4;

        is_new_qso = gtk_toggle_button_get_active
                (GTK_TOGGLE_BUTTON
                 (lookup_widget (mainwindow, "qsoisnewflag")));

	call = gtk_editable_get_chars (GTK_EDITABLE (editable), 0, -1);
        g_strstrip (call);

	GtkWidget *awardsentry = lookup_widget (mainwindow, "awardsentry");
	gchar *aw = gtk_editable_get_chars (GTK_EDITABLE (awardsentry), 0, -1);
	gchar *result = valid_awards_entry (aw, &st, &zone, &cont, &iota);
	if (!result)
		updatedxccframe (call, FALSE, st, zone, cont, iota);
	else
		g_free (result);
	g_free (aw);
	wb4 = updateb4window (call);
        color = &color_black; /* Default */
        if (is_new_qso && programstate.contest_mode)
        {
                if ((wb4 & WB4FLAG_EXACT_CURRENT))
                {
                        /* Fixme: The colors depend on the contest rules.  */
                        if ((wb4 & WB4FLAG_EXACT_SAMEBAND))
                                color = &color_grey;  /* Dupe */
                        else
                                color = &color_red;   /* Multiplier? */
                }
                else if ((wb4 & WB4FLAG_MAYBE_CALLSIGN))
                {
                        color = &color_blue;  /* Not yet worked. */
                }
        }

        gtk_widget_modify_text (GTK_WIDGET(editable), GTK_STATE_NORMAL, color);

	/* twpsk support, if no IPC ID yet, create one */
#if HAVE_SYS_SHM_H
	if (programstate.shmid == -1)
	{
		if ((programstate.shmid = shmget (KEY, SHMSIZE, IPC_CREAT | 0600)) < 0)
		{
			errorstr = g_strdup_printf
				(_("shmget failed: %s"), g_strerror (errno));
			update_statusbar (errorstr);
			g_free (errorstr);
		}
		if ((shareCall = (shmat (programstate.shmid, NULL, 0))) == (gchar *) - 1)
		{
			errorstr = g_strdup_printf
				(_("shmat failed: %s"), g_strerror (errno));
			update_statusbar (errorstr);
			g_free (errorstr);
			programstate.shmid = -1;
		}
	}
	if (programstate.shmid != -1)
		strncpy (shareCall, call, SHMSIZE);	/* put call in shm */
#endif

	g_free (call);
}


gboolean
on_callentry_unfocus (GtkEntry *callentry, GdkEventFocus *event,
                      gpointer user_data)
{
	const char *call;

	if (preferences.typeaheadfind == 1)
        {
                call = gtk_entry_get_text (callentry);
                do_typeaheadfind (call);
        }
	return FALSE;
}

void
on_awardsentry_changed (GtkEditable * editable, gpointer user_data)
{
	guint st, zone, cont, iota;

	gchar *aw = gtk_editable_get_chars (editable, 0, -1);
	gchar *result = valid_awards_entry (aw,  &st, &zone, &cont, &iota);
	if (result)
	{
		updatedxccframe (result, TRUE, st, zone, cont, iota);
		g_free (result);
	}
	else
	{
		GtkWidget *callentry = lookup_widget (mainwindow, "callentry");
		gchar *call = gtk_editable_get_chars (GTK_EDITABLE (callentry), 0, -1);
		updatedxccframe (call, FALSE, st, zone, cont, iota);
		g_free (call);
	}
	g_free (aw);
}

/* check for polling, if it is enabled use the state struct,
	 otherwise get frequency from the rig when there is no
	 default entry */
void
on_mhzbutton_clicked (GtkButton * button, gpointer user_data)
{
	GtkWidget *entry, *bandoptionmenu;
	GString *digits = g_string_new ("");
	gint bandenum;

	entry = lookup_widget (mainwindow, "bandentry");
	bandoptionmenu = lookup_widget (mainwindow, "bandoptionmenu");

	/* set optionmenu and entry to the default */
	if (strlen (preferences.defaultmhz) > 0)
	{
		gtk_entry_set_text (GTK_ENTRY (entry), preferences.defaultmhz);
		bandenum = freq2enum (preferences.defaultmhz);
		if (bandenum >= 0)
			activate_bandoption_by_enum
				(bandoptionmenu, preferences.bands, bandenum);
	}
	else /* there is no default */
	{
		if (programstate.rigfrequency != 0)
			digits = convert_frequency ();
		else
			g_string_printf (digits, "UNKNOWN");
		/* set entry and optionmenu */
		gtk_entry_set_text (GTK_ENTRY (entry), digits->str);
		bandenum = freq2enum (digits->str);
		if (bandenum >= 0)
			activate_bandoption_by_enum
				(bandoptionmenu, preferences.bands, bandenum);
		g_string_free (digits, TRUE);
	}
}

void
on_modebutton_clicked (GtkButton * button, gpointer user_data)
{
	GtkWidget *entry, *modeoptionmenu;
	gchar *mode;
	gint modeenum;

	entry = lookup_widget (mainwindow, "modeentry");
	modeoptionmenu = lookup_widget (mainwindow, "modeoptionmenu");

	/* use default */
	if (strlen (preferences.defaultmode) > 0)
	{
		gtk_entry_set_text (GTK_ENTRY (entry), preferences.defaultmode);
		modeenum = mode2enum (preferences.defaultmode);
		if (modeenum >= 0)
			activate_modeoption_by_enum
				(modeoptionmenu, preferences.modes, modeenum);
	}
	else	/* NO default */
	{
		mode = rigmode (programstate.rigmode);
		gtk_entry_set_text (GTK_ENTRY (entry), mode);
		modeenum = mode2enum (mode);
		if (modeenum >= 0)
			activate_modeoption_by_enum
				(modeoptionmenu, preferences.modes, modeenum);
		g_free (mode);
	}
}

/* fill in the rst field, if the keyerwindow is active and the default rst
   contains the '#' macro, substitute it with the counter */
void
on_rstbutton_clicked (GtkButton * button, gpointer user_data)
{
	GtkWidget *rstentry, *count;
	gint c;
	gchar *str, *countstr, *lastmsg;

	rstentry = lookup_widget (mainwindow, "rstentry");
	if (strlen (preferences.defaulttxrst) > 0)
	{
		if (keyerwindow && g_strrstr (preferences.defaulttxrst, "#"))
		{
			count = lookup_widget (keyerwindow, "count");
			c = gtk_spin_button_get_value (GTK_SPIN_BUTTON(count));
			if (c < 10)
				countstr = g_strdup_printf ("00%d", c);
			else if (c < 100)
				countstr = g_strdup_printf ("0%d", c);
			else
				countstr = g_strdup_printf ("%d", c);
			str = my_strreplace (preferences.defaulttxrst, "#", countstr);
			g_free (countstr);
			gtk_entry_set_text (GTK_ENTRY (rstentry), str);
			g_free (str);
		}
		else if (keyerwindow && g_strrstr (preferences.defaulttxrst, "~"))
		{
			lastmsg = get_last_msg ();
			str = my_strreplace (preferences.defaulttxrst, "~", lastmsg);
			g_free (lastmsg);
			gtk_entry_set_text (GTK_ENTRY (rstentry), str);
			g_free (str);
		}
		else
			gtk_entry_set_text (GTK_ENTRY (rstentry), preferences.defaulttxrst);
	}
	else if (programstate.rigrst != 0)
		gtk_entry_set_text (GTK_ENTRY (rstentry), programstate.rigrst);
	else
		gtk_entry_set_text (GTK_ENTRY (rstentry), "UNKNOWN");
}

void
on_powerbutton_clicked (GtkButton * button, gpointer user_data)
{
	GtkWidget *entry;
	gchar *rigpower;

	entry = lookup_widget (mainwindow, "powerentry");
	if (strlen (preferences.defaultpower) > 0)
		gtk_entry_set_text (GTK_ENTRY (entry), preferences.defaultpower);
	else if (programstate.rigpower != 0)
		{
			rigpower = g_strdup_printf ("%d", programstate.rigpower / 1000);
			gtk_entry_set_text (GTK_ENTRY (entry), rigpower);
			g_free (rigpower);
		}
	else
		gtk_entry_set_text (GTK_ENTRY (entry), "UNKNOWN");
}


/* check locator and calculate distance and azimuth */
void
on_locatorentry_changed (GtkEditable * editable, gpointer user_data)
{
	gchar *locator;

	locator = gtk_editable_get_chars (GTK_EDITABLE (editable), 0, -1);
	updatelocatorframe (locator);
	g_free (locator);

	/* Update Locator Award */

	guint st, zone, cont, iota;
	GtkWidget *awardsentry = lookup_widget (mainwindow, "awardsentry");
	gchar *aw = gtk_editable_get_chars (GTK_EDITABLE (awardsentry), 0, -1);
	gchar *result = valid_awards_entry (aw,  &st, &zone, &cont, &iota);
	if (result)
	{
		updatedxccframe (result, TRUE, st, zone, cont, iota);
		g_free (result);
	}
	else
	{
		GtkWidget *callentry = lookup_widget (mainwindow, "callentry");
		gchar *call = gtk_editable_get_chars (GTK_EDITABLE (callentry), 0, -1);
		updatedxccframe (call, FALSE, st, zone, cont, iota);
		g_free (call);
	}
	g_free (aw);
}

void
tv_changed (GtkTextBuffer *buffer, gpointer user_data)
{
	gint count;

	count = gtk_text_buffer_get_char_count (buffer);
	if (count > 512)
	{
		update_statusbar (_("Warning, remarks line to long!!"));
		gdk_beep ();
	}
}

/* entry get's focus because Alt+key is used */
void
entry_mnemonic_activate (GtkWidget *entry, gboolean arg1, gpointer user_data)
{
	const char *temp;

	switch (GPOINTER_TO_INT(user_data))
        {
        case QSO_FIELD_BAND:  temp = preferences.defaultmhz; break;
	case QSO_FIELD_MODE:  temp = preferences.defaultmode; break;
        case QSO_FIELD_TXRST: temp = preferences.defaulttxrst; break;
        case QSO_FIELD_RXRST: temp = preferences.defaultrxrst; break;
        case QSO_FIELD_AWARDS:temp = preferences.defaultawards; break;
	case QSO_FIELD_POWER: temp = preferences.defaultpower; break;
        case QSO_FIELD_UNKNOWN1: temp = preferences.defaultfreefield1; break;
        case QSO_FIELD_UNKNOWN2: temp = preferences.defaultfreefield2; break;
        default: temp = NULL; break;
        }

	if (temp)
		gtk_entry_set_text (GTK_ENTRY(entry), temp);
	gtk_widget_grab_focus (entry);
	gtk_editable_set_position (GTK_EDITABLE(entry), -1);
}


/* textview get's focus because Alt+key is used */
void
tv_mnemonic_activate (GtkWidget *tv, gboolean arg1, gpointer user_data)
{
	if (strlen (preferences.defaultremarks) > 0)
	{
		GtkTextBuffer *b = gtk_text_view_get_buffer (GTK_TEXT_VIEW (tv));
		gtk_text_buffer_set_text (b, preferences.defaultremarks, -1);
		gtk_widget_grab_focus (tv);
	}
}

/* bandcombobox get's focus because Alt+m is used */
void
bandoptionactivate (GtkWidget *bandoptionmenu, gboolean arg1, gpointer user_data)
{
	gint bandenum;

	if (strlen (preferences.defaultmhz) > 0)
	{
		bandenum = freq2enum (preferences.defaultmhz);
		if (bandenum >= 0)
			activate_bandoption_by_enum
				(bandoptionmenu, preferences.bands, bandenum);
		gtk_widget_grab_focus (bandoptionmenu);
	}
}


/* modecombobox get's focus because Alt+o is used */
void
modeoptionactivate (GtkWidget *modeoptionmenu, gboolean arg1, gpointer user_data)
{
	gint modeenum;

	if (strlen (preferences.defaultmode) > 0)
	{
		modeenum = mode2enum (preferences.defaultmode);
		if (modeenum >= 0)
			activate_modeoption_by_enum
				(modeoptionmenu, preferences.modes, modeenum);
		gtk_widget_grab_focus (modeoptionmenu);
	}
}


/* This is used by the dedicated save button and differs from
 * on_abutton_clicked (Write-QSO) in that it checks that all required
 * values haven been entered and and otherwise marks the missing
 * values.  */
void
on_save_button_clicked (GtkButton * button, gpointer user_data)
{
        GtkWidget *dateentry, *gmtentry, *callentry, *bandentry;
        GtkWidget *modeentry, *rstentry, *myrstentry;
        GtkWidget *thisentry;
        GtkWidget *errwidget = NULL;
        const char *errstr = NULL;
        char *call;
        char *buf;

        /* Avoid any checks if this is not a new QSO.  And of course
         * do an update instead of a new QSO.  */
        if (!gtk_toggle_button_get_active
            (GTK_TOGGLE_BUTTON (lookup_widget (mainwindow, "qsoisnewflag"))))
        {
                on_ubutton_clicked (button, user_data);
                return;
        }

        dateentry = lookup_widget (mainwindow, "dateentry");
        gmtentry = lookup_widget (mainwindow, "gmtentry");
        callentry = lookup_widget (mainwindow, "callentry");
        bandentry = lookup_widget (mainwindow, "bandentry");
        modeentry = lookup_widget (mainwindow, "modeentry");
        rstentry = lookup_widget (mainwindow, "rstentry");
        myrstentry = lookup_widget (mainwindow, "myrstentry");

        thisentry = callentry;
        call = gtk_editable_get_chars (GTK_EDITABLE (thisentry), 0, -1);
        if (!call || !*g_strstrip (call))
        {
                errstr = "No callsign given";
                errwidget = thisentry;
                gtk_entry_set_icon_from_stock (GTK_ENTRY (callentry),
                                               GTK_ENTRY_ICON_SECONDARY,
                                               GTK_STOCK_DIALOG_WARNING);
        }
        else if (strlen (call) < 3)
        {
                errstr = "Callsign is too short";
                errwidget = thisentry;
                gtk_entry_set_icon_from_stock (GTK_ENTRY (callentry),
                                               GTK_ENTRY_ICON_SECONDARY,
                                               GTK_STOCK_DIALOG_WARNING);
        }
        else if (strpbrk (call, " \t\n\r\v"))
        {
                errstr = "Spaces in callsign";
                errwidget = thisentry;
                gtk_entry_set_icon_from_stock (GTK_ENTRY (callentry),
                                               GTK_ENTRY_ICON_SECONDARY,
                                               GTK_STOCK_DIALOG_WARNING);
        }
        g_free (call);

        if (programstate.contest_mode)
        {
                thisentry = rstentry;
                buf = gtk_editable_get_chars (GTK_EDITABLE (thisentry), 0, -1);
                if (!buf || !*g_strstrip (buf))
                {
                        if (!errstr)
                        {
                                errstr = _("No RST sent?");
                                errwidget = thisentry;
                        }
                        gtk_entry_set_icon_from_stock (GTK_ENTRY (thisentry),
                                                       GTK_ENTRY_ICON_SECONDARY,
                                                       GTK_STOCK_DIALOG_WARNING);
                }
                g_free (buf);

                thisentry = myrstentry;
                buf = gtk_editable_get_chars (GTK_EDITABLE (thisentry), 0, -1);
                if (!buf || !*g_strstrip (buf))
                {
                        if (!errstr)
                        {
                                errstr = _("No RST received?");
                                errwidget = thisentry;
                        }
                        gtk_entry_set_icon_from_stock (GTK_ENTRY (thisentry),
                                                       GTK_ENTRY_ICON_SECONDARY,
                                                       GTK_STOCK_DIALOG_WARNING);
                }
                g_free (buf);
        }


        buf = gtk_editable_get_chars (GTK_EDITABLE (gmtentry), 0,-1);
        if (!buf || !*g_strstrip (buf))
                insert_time ();
        /* else if () fixme check  that the given time is valid.  */
        g_free (buf);


        if (errstr)
        {
                if (errwidget)
                        gtk_widget_grab_focus (errwidget);
                update_statusbar (errstr);
        }
        else
                on_abutton_clicked (button, user_data);
}
