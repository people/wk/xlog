/* globals.h - Declarartion of global variables

   xlog - GTK+ logging program for amateur radio operators
   Copyright (C) 2001 - 2008 Joop Stakenborg <pg4i@amsat.org>

   This file is part of xlog.

   Xlog is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Xlog is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with xlog.  If not, see <https://www.gnu.org/licenses/>.
   SPDX-License-Identifier: GPL-3.0-or-later
*/

#ifndef XLOG_GLOBALS_H
#define XLOG_GLOBALS_H 1

#include "main.h"

/*
 * Globals which are first initialized by main.c
 */

/* The main window with the QSO frame and the logbook listing.  */
GtkWidget *mainwindow;

/* The notebook in the main window with the log files.  */
GtkWidget *mainnotebook;

/* The list of logwindows in the mainnotebook.  */
GList *logwindowlist;

/* The state of the program (type is defined in main.h).  */
programstatetype programstate;




#endif /*XLOG_GLOBALS_H*/
