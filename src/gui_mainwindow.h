/*

   xlog - GTK+ logging program for amateur radio operators
   Copyright (C) 2001 - 2008 Joop Stakenborg <pg4i@amsat.org>

   This file is part of xlog.

   Xlog is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Xlog is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with xlog.  If not, see <http://www.gnu.org/licenses/>.

*/


/* Field number used for some fields of the QSO frame.  */
enum
{
        QSO_FIELD_BAND     = 1,
        QSO_FIELD_MODE     = 2,
        QSO_FIELD_TXRST    = 3,
        QSO_FIELD_RXRST    = 4,
        QSO_FIELD_AWARDS   = 5,
        QSO_FIELD_POWER    = 6,
        QSO_FIELD_UNKNOWN1 = 7,
        QSO_FIELD_UNKNOWN2 = 8
};


const char *get_qso_frame_field_order_help (void);
void update_qso_frame_field_order (GtkWidget *window);
void update_qso_frame_header (GtkWidget *window, const char *string);
GtkWidget* create_mainwindow (void);
